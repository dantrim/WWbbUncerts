//WWbbUncerts
#include "WWbbUncerts/WWbb2Loop.h"
#include "WWbbUncerts/utility/misc_helpers.h"

//xAOD
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "PathResolver/PathResolver.h"
#include "xAODMetaData/FileMetaData.h"
#include "xAODBase/IParticleHelpers.h" // setOriginalObjectLink

//std/stl
#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

//ROOT
#include "TFile.h"
#include "TLorentzVector.h"

const static SG::AuxElement::ConstAccessor<char> acc_bad("bad");
const static SG::AuxElement::ConstAccessor<char> acc_cosmic("cosmic");
const static SG::AuxElement::ConstAccessor<char> acc_baseline("baseline");
const static SG::AuxElement::ConstAccessor<char> acc_signal("signal");
const static SG::AuxElement::ConstAccessor<char> acc_passOR("passOR");
const static SG::AuxElement::ConstAccessor<char> acc_bjet("bjet");

const static SG::AuxElement::Decorator<int> dec_charge("MyCharge");
const static SG::AuxElement::ConstAccessor<int> acc_charge("MyCharge");

const static SG::AuxElement::ConstAccessor<float> acc_z0sinTheta("z0sinTheta");
const static SG::AuxElement::ConstAccessor<float> acc_d0sig("d0sig");
