//util
//#include "WWWbbUncerts/
#include "WWbbUncerts/utility/ChainHelper.h"

//std/stl
#include <iostream>
#include <fstream>
using namespace std;

//boost
#include <boost/program_options.hpp>

//xAOD
#include "xAODRootAccess/Init.h"

int main(int argc, char** argv)
{
    string ALG = string(argv[0]);
    string _input_file = "";
    long long _n_to_process = -1;
    bool _debug = false;

    namespace po = boost::program_options;
    string description = "Run WWbbUncerts Analysis";
    po::options_description desc(description);
    desc.add_options()
    desc.add_options()
        ("help,h", "print out the help message")
        ("input,i",
            po::value(&_input_file),
                "input file [DAOD, text filelist, or directory of DAOD files]")
        ("nentries,n",
            po::value(&_n_to_process)->default_value(-1),
                "number of entries to process [default: all]")
        ("verbose,v",
            po::value(&_debug),
                "trun on verbose mode")
    ;
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
    po::notify(vm);

    if(vm.count("help"))
    {
        cout << desc << endl;
        return 0;
    }

    if(!vm.count("input"))
    {
        cerr << "ERROR You did not provide an input file/list/dir" << endl;
        return 1;
    }

    std::ifstream user_input_fs(_input_File);
    if(!user_input_fs.good())
    {
        cerr << "ERROR Provided input (" << _input_file << ") not found" << endl;
        return 1;
    }

    xAOD::Init("WWbbUncerts");
    TChain* chain = new TChain("CollectionTree");
    int file_err = ChainHelper::addInput(chain, _input_file, true);
    if(file_err) return 1;
    chain->ls();
    Long64_t n_in_chain = chain->GetEntries();
    if(_n_to_process < 0) _n_to_process = n_in_chain;
    if(_n_to_process > n_in_chain) _n_to_process = n_in_chain;

    // build looper
    cout << "=================================================================" << endl;
    cout << ALG << "    Total number of entries in the input chain : " << n_in_chain << endl;
    cout << ALG << "    Total number of entries to process         : " << _n_to_process << endl;
    cout << "=================================================================" << endl;


    return 0;
}
