#ifndef WWBB_UNCERTS_LOOP_H
#define WWBB_UNCERTS_LOOP_H

//ROOT
#include "TSelector.h"
#include "TStopwatch.h"
#include "TTree.h"
#include "TChain.h"

//xAOD
#include "xAODRootAccess/tools/ReturnCheck.h"
#include "xAODEventInfo/EventInfo.h"
namespace xAOD {
    class TEvent;
    class TStore;
}

//tools
#include "SUSYTools/SUSYObjDef_xAOD.h"

//CONTAINERS
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"

//ASG
#include "AsgTools/ToolHandle.h"
#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#include "MuonAnalysisInterfaces/IMuonEfficiencyScaleFactors.h"
#include "EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h"
#include "EgammaAnalysisInterfaces/IAsgElectronEfficiencyCorrectionTool.h"
#include "FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h"
#include "FTagAnalysisInterfaces/IBTaggingSelectionTool.h"
#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"

//std/stl
#include <map>

namespace wwbb
{

struct pt_greater {
    bool operator() (const xAOD::IParticle* a, const xAOD::IParticle* b) { return a->pt() > b->pt(); }
};
static pt_greater PtGreater;

struct pt_greaterJet {
    bool operator() (const xAOD::Jet* a, const xAOD::Jet* b) { return a->pt() > b->pt(); }
};
static pt_greaterJet PtGreaterJet;

class WWbbUncertsLoop : public TSelector
{
    public :
        WWbbUncertsLop();
        virtual ~WWbbUncertsLoop(){};

        void set_verbose(bool doit) { m_dbg = doit; }
        bool dbg() { return m_dbg; }

        ST::SUSYObjDef_xAOD* susyObj() { return _susyObj; }

        xAOD::TEvent* event() { return m_event; }
        xAOD::TStore* store() { return m_store; }

         bool initialize_susytools();

         //////////////////////////////////////////////////////
         // TSelector overrides
         //////////////////////////////////////////////////////
         virtual void Init(TTree* tree); // Called every time a new TTree is attached
         //virtual void SlaveBegin(TTree* tree);
         virtual void Begin(TTree* tree); // Called before looping on entries
         virtual Bool_t Notify() { return kTRUE; } // Called at the first entry of a new file
         virtual void Terminate(); // called after looping is finished
         virtual Int_t Version() const { return 2; } // adhere to stupid ROOT design
         virtual Bool_t Process(Long64_t entry); // main event loop function

         //////////////////////////////////////////////////////
         // Object Getters
         //////////////////////////////////////////////////////
         xAOD::ElectronContainer* xaod_electrons() { return _electrons; }
         xAOD::MuonContainer* xaod_muons() { return _muons; }
         xAOD::JetContainer* xaod_jets() { return _jets; }
         xAOD::MissingETContainer* xaod_met() { return _met; }
         xAOD::PhotonContainer* xaod_photons() { return _photons; }
         xAOD::TauJetContainer* xaod_taus() { return _taus; }
         
         std::vector<xAOD::IParticle*> base_electrons() { return _base_electrons; }
         std::vector<xAOD::IParticle*> sig_electrons() { return _sig_electrons; }
         std::vector<xAOD::IParticle*> pre_muons() { return _pre_muons; }
         std::vector<xAOD::IParticle*> base_muons() { return _base_muons; }
         std::vector<xAOD::IParticle*> sig_muons() { return _sig_muons; }
         std::vector<xAOD::IParticle*> base_leptons() { return _base_leptons; }
         std::vector<xAOD::IParticle*> sig_leptons() { return _sig_leptons; }
         std::vector<xAOD::Jet*> base_jets() { return _base_jets; }
         std::vector<xAOD::Jet*> sig_jets() { return _sig_jets; }
         std::vector<xAOD::Photon*> base_photons() { return _base_photons; }
         std::vector<xAOD::Photon*> sig_potons() { return _sig_photons; }
         std::vector<xAOD::TauJet*> base_taus() { return _base_taus; }
         std::vector<xAOD::TauJet*> sig_taus() { return _sig_taus; }

    private :

        bool m_dbg;

        TStopwatch m_timer;
        
        //xAOD
        xAOD::TEvent* m_event;
        xAOD::TStore* m_store;
        
        //TOOLS
        ST::SUSYObjDef_xAOD* _susyObj;
        asg::AnaToolHandle<IAsgElectronLikelihoodTool> _ele_idtool_baseline;
        asg::AnaToolHandle<IAsgElectronLikelihoodTool> _ele_idtool_signal;
        asg::AnaToolHandle<CP::IMuonSelectionTool> _mu_idtool_baseline;
        asg::AnaToolHandle<CP::IMuonSelectionTool> _mu_idtool_signal;
        asg::AnaToolHandle<IBTaggingSelectionTool> _btag_sel_tool;
        asg::AnaToolHandle<IBTaggingEfficiencyTool> _btag_eff_tool;
        asg::AnaToolHandle<IGoodRunsListSelectionTool> _grl_tool;
        
        //object containers
        xAOD::ElectronContainer* _electrons;
        xAOD::ShallowAuxContainer* _electrons_aux;
        xAOD::MuonContainer* _muons;
        xAOD::ShallowAuxContainer* _muons_aux;
        xAOD::JetContainer* _jets;
        xAOD::ShallowAuxContainer* _jets_aux;
        xAOD::MissingETContainer* _met;
        xAOD::MissingETAuxContainer* _met_aux;
        xAOD::PhotonContainer* _photons;
        xAOD::ShallowAuxContainer* _photons_aux;
        xAOD::TauJetContainer* _taus;
        xAOD::ShallowAuxContainer* _taus_aux;
        
        
        std::vector<xAOD::IParticle*> _base_electrons;
        std::vector<xAOD::IParticle*> _sig_electrons;
        std::vector<xAOD::IParticle*> _pre_muons;
        std::vector<xAOD::IParticle*> _base_muons;
        std::vector<xAOD::IParticle*> _sig_muons;
        std::vector<xAOD::IParticle*> _base_leptons;
        std::vector<xAOD::IParticle*> _sig_leptons;
        std::vector<xAOD::Jet*> _base_jets;
        std::vector<xAOD::Jet*> _sig_jets;
        std::vector<xAOD::Photon*> _base_photons;
        std::vector<xAOD::Photon*> _sig_photons;
        std::vector<xAOD::TauJet*> _base_taus;
        std::vector<xAOD::TauJet*> _sig_taus;


}; // class WWbbUncertsLoop

} // namespace wwbb

#endif
