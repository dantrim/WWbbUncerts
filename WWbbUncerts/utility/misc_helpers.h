#ifndef WWBB_MISC_HELPERS_H
#define WWBB_MISC_HELPERS_H

#include <string>

std::string computeMethodName(const std::string& function, const std::string& prettyFunction);
#define __hhfunc__ computeMethodName(__FUNCTION__,__PRETTY_FUNCTION__).c_str()

#endif
